// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', 
  [
    'ionic', 
    'luegg.directives',
    'localStorage',
    'i18n'
])

.run(function($ionicPlatform, $ionicSideMenuDelegate, storage, $translate) {
  $ionicPlatform.ready(function() {
    // load stored params to activate if needed the insomnia
    var params = storage.getStoredParams();
    try{
      if (params && params.insomnia) {
        window.plugins.insomnia.keepAwake();
      } else {
        window.plugins.insomnia.allowSleepAgain();
      }
    } catch(e){
      // appends when running on desktop
    }

    // load language
    if (params && params.language){
      $translate.use(params.language);
    }

    // load font size for html root element
    if (params.fontSize){
      angular.element("html").css("font-size", params.fontSize);
    }


    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    // dealing with android menu button
    if (window.cordova) {
      document.addEventListener("menubutton", this.onHardwareMenuKeyDown, false);
    }
  });

  this.onHardwareMenuKeyDown = function () {
    $ionicSideMenuDelegate.toggleLeft();
  }
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'MenuCtrl as menu'
    })

    .state('app.players', {
      url: "/players",
      views: {
        'menuContent' :{
          templateUrl: "templates/players.html",
          controller: 'PlayersCtrl as ctrl', 
        }
      },
    })


    .state('app.scores', {
      url: "/scores",
      views: {
        'menuContent' :{
          templateUrl: "templates/scores.html",
          controller: 'ScoresCtrl as scoresCtrl'
        }
      }
    })

    .state('app.params', {
      url: "/params",
      views: {
        'menuContent' :{
          templateUrl: "templates/parametres.html",
          controller: 'ParametresCtrl as paramsCtrl'
        }
      }
    })
    
    .state('app.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: "templates/about.html"
        }
      }
    })
    ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/scores');
});
