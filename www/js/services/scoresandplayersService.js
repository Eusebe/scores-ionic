'use strict';

/**
 * @ngdoc service
 * @name scoresrefactorApp.scoresAndPlayers
 * @description
 * # scoresAndPlayers
 * Service in the scoresrefactorApp.
 */
angular.module('starter')
  .service('scoresAndPlayersService', ['scoresFactory', 'storage', function (scoresFactory, storage) {

    // AngularJS will instantiate a singleton by calling "new" on this function
    var myService = {};

    myService.scores = {};
    myService.players = {};

    myService.setPlayers = function(playersArray) {
      try{
        this.players = playersArray;
        this.scores = [];
        this.scores.push(scoresFactory.getArrayForPlayers(playersArray));
        for (var int = 0; int < playersArray.length; int++) {
          playersArray[int].total = 0;
        }
        storage.storePlayers(playersArray);
        storage.clearStoredScores();
      } catch(e){
        console.log(e);
      }
    };

    myService.initPlayers = function() {
      var players = storage.getStoredPlayers();
      var scores = storage.getStoredScores();
      if (!players) {
        players = [
                    {name:'joueur 1', total:0}, 
                    {name:'joueur 2', total:0}, 
                  ];
        // sets myService.players + initialize myService.scores
        myService.setPlayers(players);

        // updates local storage
        storage.storePlayers(players);
        storage.clearStoredScores();
      } else {
        if (!scores){
          myService.setPlayers(players);
        } else {
          myService.players = players;
          myService.scores = scores;
        }
      }
    };

    myService.initPlayers();

    return myService;


  }

 ]);
