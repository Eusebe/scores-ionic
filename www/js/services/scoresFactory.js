'use strict';
/**
 * @ngdoc service
 * @name scoresrefactorApp.scoresFactory
 * @description
 * # scoresFactory
 * Factory in the scoresrefactorApp.
 */
 angular.module('starter')
 .factory('scoresFactory', function () {

    // Public API here
    return {

      // update the total for the player designed by the index
      updateTotal : function(index, playersArray, scoresArray){
        // clean totals
        playersArray[index].total = 0;
        // calculate total
        for (var j = 0; j < scoresArray.length; j++) {
          var currentValue = scoresArray[j][index].value;
          //          console.log("cell #" + i + ', ' + j + ': ' + currentValue);
          if (currentValue){
            playersArray[index].total += currentValue;
                  // console.log("new total:" + this.players[index].total);
          }
        }
      },

      // if the last line is filled, add a new empty row
      addRowIfRequired : function(scoresArray, playersArray){
        var isLastLineFull = true;
        var lastLineIndex = scoresArray.length -1;
        for (var i = 0; i < scoresArray[lastLineIndex].length; i++) {
          isLastLineFull = isLastLineFull && !this.isEmpty(scoresArray[lastLineIndex][i].value);
        }
        //console.log("is last line full: " + isLastLineFull);
        if (isLastLineFull){
          // add a new empty row
          scoresArray.push(playersArray);
        }
        return isLastLineFull;
      },

      //
      isEmpty : function(value){
        return value ===null || isNaN(value);
      },

      //
      removeLastEmptyRow : function(scoresArray){
        // console.log("#removeLastEmptyRow");
        var lastLineIndex = scoresArray.length -1;
        if (lastLineIndex == 0) return;
        var isLastLineEmpty = true;
        for (var i = 0; i < scoresArray.length; i++) {
          isLastLineEmpty = isLastLineEmpty && isEmpty(scoresArray[lastLineIndex][i].value);
        }
        console.log("last row removed ? " + isLastLineEmpty);
        if(isLastLineEmpty){
          scoresArray.pop();
        }
      },

      getArrayForPlayers : function(playersArray){
        // console.log("#getArraysForPlayers");
        var array = new Array();
        for (var i = 0; i < playersArray.length; i++) {
          array.push({value:null, col:i});
        }
        return array;
      },
  };
});