localStorageModule = angular.module('localStorage', []);
localStorageModule.factory("storage", storage);

function storage($window, $rootScope) {

	return {
		store: function(key, value){
			return $window.localStorage
				&& $window.localStorage.setItem(key, JSON.stringify(value));
		},
		get: function(key){
			return $window.localStorage
				&& JSON.parse($window.localStorage.getItem(key));
		},
		clear: function(key){
			return $window.localStorage
				&& $window.localStorage.removeItem(key); 
		},

		/* specific methods */

		storePlayers: function(players){
			return this.store('players', players);
		},
		getStoredPlayers: function(){
			return this.get('players');
		},
		clearStoredPlayers: function(){
			return this.clear('players');
		},

		storeScores: function(scores){
			return this.store('scores', scores);
		},
		getStoredScores: function(){
			return this.get('scores');
		},
		clearStoredScores: function(){
			return this.clear('scores');
		},
		
		storeParams: function(params){
			return this.store('parameters', params);
		},
		getStoredParams: function(){
			return this.get('parameters');
		},
		clearStoredParams: function(){
			return this.clear('parameters');
		},

	};
};
