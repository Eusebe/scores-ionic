angular.module('i18n', ['pascalprecht.translate'])
.config(function($stateProvider, $urlRouterProvider, $translateProvider) {
  $translateProvider.translations('en', {
    PARAMS: "Parameters",
    PLAYERS: "Players",
    SCORES: "Scores",
    ABOUT: "About",
    CLEAR_SCORES_MENU_ITEM : "Clear scores",
    PREVENT_SLEEP: "Keep awake",
    RESET:"Reset application",
    OK_BUTTON: "OK",
    CANCEL_BUTTON:"Cancel",
    MENU:"Menu",
    LANGUAGE: "Language",
    CLEAR_ALL:"Do you really want to clear all data (parameters, players, scores) ?",
    CLEAR_PLAYERS:"Do you really want to clear players ?\nYou will lose current scores.",
    CLEAR_SCORES:"Do you really want to clear scores ?",
    FONT_SIZE:"Font size",
    small:"small",
    medium: "medium",
    big: "big",
    huge:"huge",
    MADE_WITH:"made with "
  });
  $translateProvider.translations('fr', {
    PARAMS: "Paramètres",
    PLAYERS: "Joueurs",
    SCORES: "Scores",
    ABOUT: "à propos",
    CLEAR_SCORES_MENU_ITEM : "Réinitialiser les scores",
    PREVENT_SLEEP: "Empêcher la mise en veille",
    RESET:"Réinitialiser l'application",
    OK_BUTTON: "OK",
    CANCEL_BUTTON:"Annuler",
    MENU:"Menu",
    LANGUAGE: "Langue",
    CLEAR_ALL: "Êtes-vous sûr de vouloir supprimer tous les données (paramètres, joueurs, scores) ?",
    CLEAR_PLAYERS:'Êtes-vous sûr de vouloir réinitialiser les joueurs ?\nCeci entraînera la perte des scores actuels.',
    CLEAR_SCORES:'Êtes-vous sûr de vouloir réinitialiser les scores ?',
    FONT_SIZE: "Taille des caractères",
    small:"petite",
    medium: "moyenne",
    big: "grande",
    huge: "très grande",
    MADE_WITH:"créé avec "
  });
  $translateProvider.preferredLanguage("fr");
  $translateProvider.fallbackLanguage("en");
});