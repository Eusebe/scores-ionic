'use strict';

/**
 * @ngdoc filter
 * @name scoresrefactorApp.filter:scoresFilters
 * @function
 * @description
 * # scoresFilters
 * Filter in the scoresrefactorApp.
 */
angular.module('starter')
  .filter('lenghtMaxFilter', function () {
    return function (input, length) {
      return input.substring(0,length);
    };
  });
