'use strict';

/**
 * @ngdoc function
 * @name scoresrefactorApp.controller:ScoresCtrl
 * @description
 * # ScoresCtrl
 * Controller of the scoresrefactorApp
 */
angular.module('starter')
  .controller('ScoresCtrl', ['scoresFactory', 'scoresAndPlayersService', 'storage', '$state', '$translate', '$scope', function (Factory, Service, storage, $state, $translate, $scope) {

	this.players = Service.players;
	this.scores = Service.scores;

	var refreshTotal = function(players, scores){
		for (var i = players.length - 1; i >= 0; i--) {
			Factory.updateTotal(i, players, scores);
		};
	};

	/**
	 * 
	 */
	this.valueChanged = function(cell){
		if (!Factory.isEmpty(cell.value)){
			Factory.addRowIfRequired(this.scores, Factory.getArrayForPlayers(this.players));
		} else {
			Factory.removeLastEmptyRow(this.scores);
		}
		Factory.updateTotal(cell.col, this.players, this.scores);
		storage.storeScores(Service.scores);
	};

	this.resetScores = function(){
		resetScoresClosure(this);
	};

	var resetScoresClosure = function(ctrl){
		$translate('CLEAR_SCORES').then(function(message){
			if (confirm(message)){
				this.scoresCtrl.resetScoresWithoutConfirm();			
			}
		});
	};

	this.gotoScores = function(){
		$state.transitionTo('app.scores');
	};

	// listening to 'clearScores' event
	$scope.$on('clearScores', function(event){
		$scope.scoresCtrl.resetScoresWithoutConfirm();
	});

	this.resetScoresWithoutConfirm = function(){
		Service.setPlayers(this.players);
		this.scores = Service.scores;
	};

	/* on startup */
	refreshTotal(this.players, this.scores);

  }
]);
