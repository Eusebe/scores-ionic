angular.module('starter')
.controller('ParametresCtrl', ['scoresAndPlayersService', 'storage', '$state', '$translate', function(Service, storage, $state, $translate){

	this.languagesAvailable = [{key:'fr', value:'Français'}, {key:'en', value:'English'}];

	this.fontSizes = [{key:"16px", value:"small"}
	, {key:"18px", value:"medium"}
	, {key:"21px", value:"big"}
	, {key:"24px", value:"huge"}
	];


	this.loadParams = function() {
		this.params = storage.getStoredParams() || {};
	};

	this.validate = function(){
		storage.storeParams(this.params);
	};

	this.clearAll = function(){
		clearAllClosure(this);
	};

	var clearAllClosure = function(ctrl){
		$translate('CLEAR_ALL').then(function(message){
			if (confirm(message)){
	 			storage.clearStoredParams();
				storage.clearStoredPlayers();
				storage.clearStoredScores();
				Service.initPlayers();
				ctrl.loadParams();
				ctrl.insomniaChanged();
				$state.transitionTo('app.scores');
			}
		});
	};

	this.insomniaChanged = function(){
		this.validate();
		console.log('insomnia is ' + (this.params.insomnia ? 'on' : 'off'));
		if (this.params.insomnia){
			try{window.plugins.insomnia.keepAwake();}catch(e){}
		}else if (!this.params.insomnia){
			try{window.plugins.insomnia.allowSleepAgain();}catch(e){}
		}
	};

	this.languageChanged = function(){
		this.validate();
		if (this.params.language === 'fr' || this.params.language === 'en'){
			$translate.use((this.params.language).split("-")[0]).then(function(data) {
                console.log("SUCCESS -> " + data);
            }, function(error) {
                console.log("ERROR -> " + error);
            });
		}
	};

	this.fontSizeChanged = function(){
		angular.element("html").css("font-size", this.params.fontSize);
		this.validate();
	};

	this.loadParams();
	this.insomniaChanged();
}]);