angular.module('starter')
 .controller('MenuCtrl', ['scoresAndPlayersService', '$rootScope', '$state', '$translate',  function (Service, $rootScope, $state, $translate) {
	this.notifyClearScores = function(){
//		notifyClearScoresClosure(this);
		$translate('CLEAR_SCORES').then(function(message){
			if (confirm(message)){
				$rootScope.$broadcast('clearScores'); // si on est sur la vue scores
				Service.setPlayers(Service.players); // si on est sur une autre vue
				$state.transitionTo('app.scores');
			}
		});

	};

/*	var notifyClearScoresClosure = function(ctrl){
	}; */
 }]);
