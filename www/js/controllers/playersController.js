'use strict';

/**
 * @ngdoc function
 * @name scoresrefactorApp.controller:PlayersCtrl
 * @description
 * # PlayersCtrl
 * Controller of the scoresrefactorApp
 */
angular.module('starter')
  .controller('PlayersCtrl', ['scoresAndPlayersService', '$state', '$ionicPopup', 'storage', '$translate',  function (Service, $state, $ionicPopup, storage, $translate) {
	/**
	 * 
	 */
	this.addNewPlayerIfRequired = function(){
		// console.log("#addNewPlayerIfRequired");
		var needNewPlayer = true;
		for (var i = 0; i < this.copyOfPlayers.length; i++) {
			var player = this.copyOfPlayers[i];
			// console.log("#" + player.name + "#");
			needNewPlayer = needNewPlayer && player.name;
		};
		if (needNewPlayer) {
			// add a new row to the temporary players list
			this.copyOfPlayers.push({name:'', total:0});
		};
	};

	this.showConfirm = function() {
		showConfirmClosure(this);
	};

	var showConfirmClosure = function(ctrl){
		$translate('CLEAR_PLAYERS').then(function(message){
			if(confirm(message)) {
				ctrl.clearScores();
				// TODO: store locally players
				storage.storePlayers(Service.players);
			} 
		});
	};

	this.clearScores = function(){
		// console.log("#clearScores");
		this.removeEmptyPlayers(this.copyOfPlayers);
		Service.setPlayers(this.copyOfPlayers);
		this.players = this.copyOfPlayers;
		this.scores = Service.scores;
		this.gotoScores();
		// console.log('#scores=' + this.scores);
	};


	
	this.removeEmptyPlayers = function(elements){
		for (var i = elements.length -1; i >= 0; i--) {
			if (elements[i].name == ''){
				elements.splice(i,1);
			}
		}
	};


	this.isDownable = function(array, index){
		return index < array.length-1 && array[index+1].name != '';
	};

		/**
	 * remove the player of the temporary players list.
	 */
	this.removePlayer = function(player){
		player.name='';
		for (var i = 0; i < this.copyOfPlayers.length; i++) {
			if (!this.copyOfPlayers[i].name){
				this.copyOfPlayers.splice(i, 1);
			}
		}
		this.addNewPlayerIfRequired();
		console.log(this.copyOfPlayers.length);
	};
	
	/**
	 * copyOfPlayers = temporary copy of players to be updated on popup validation (or to be re-initialized on click out of the popup).
	 */
	this.refreshPlayers = function(){
		this.copyOfPlayers = angular.copy(this.players);
		this.addNewPlayerIfRequired();
	};
	
	
	this.downPlayer = function(index){
		var array = this.copyOfPlayers;
		if (this.isDownable(array, index)){
			var temp = array[index];
			array[index] = array[index+1];
			array[index+1] = temp;
		}
	};

	
	this.upPlayer = function(index){
		if (index > 0) {
			var temp = this.copyOfPlayers[index];
			this.copyOfPlayers[index] = this.copyOfPlayers[index-1];
			this.copyOfPlayers[index-1] = temp;
		}
	};


	this.gotoScores = function(){
		$state.transitionTo('app.scores');
	};

	this.players = Service.players;
	this.copyOfPlayers = angular.copy(this.players);
	this.addNewPlayerIfRequired();
  }]);
